import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import Cat from 'App/Models/Cat'

export default class CatsController {
    public async index(ctx: HttpContextContract) {
        console.log("Http url : ", ctx.request.url())
        // const cats = await Cat.findMany([1, 2]);
        return [
            {
                title: "Liste des routes",
                route: "/",
                method: "GET",
            },
            {
                title: 'Avoir la liste des chats',
                route: "/cats",
                method: "GET",
            },
            {
                title: 'Ajouter des chats',
                route: '/cats/create',
                method: "POST",
            },
            {
                title: 'Recuperer un chat',
                route: '/cats/:id',
                method: "GET",
            },
            {
                title: "Recuperer les infos d'un chat pour modifier",
                route: '/cats/:id/edit',
                method: "GET",
            },
            {
                title: "Modifier les infos d'un chat venant du formulaire",
                route: "/cats/:id",
                method: "PUT",
            },
            {
                title: "Modifier les infos d'un chat venant du formulaire",
                route: "/cats/:id",
                method: "DELETE",
            },
        ]
    }

    /**
     * getAllCats
     */
    public async getAllCats() {
        return Database.from('cats').select('*')
    }

    /**
     * createCat
     */
    public async createCat({ request, response }: HttpContextContract) {
        const name = request.input('name');
        const breed = request.input('breed');
        const color = request.input('color');
        const age = request.input('age');

        const cat = await Cat.create({ name, breed, color, age })

        return response.json({ cat });
    }

    /**
     * getCatById
     */
    public async getCatById({ params, response }: HttpContextContract) {
        const id = params.id;

        const cat = await Cat.query().where('id', id).firstOrFail()

        return response.json({ cat });
    }

    /**
     * getCatForEdit
     */
    public async getCatForEdit({ params, response }: HttpContextContract) {
        const id = params.id;

        const cat = await Cat.query().where('id', id).first();

        return response.json({ cat });
    }

    /**
     * updateCat
     */
    public async updateCat({ request, response, params }: HttpContextContract) {
        // const cat = await Cat.findByOrFail('id', params.id)

        // cat.name = request.input('name');
        // cat.breed = request.input('breed');
        // cat.color = request.input('color');
        // cat.age = request.input('age');

        // await cat.save();

        const data = request.only(['name', 'breed', 'color', 'age'])
        const cat = await Database.from('cats').where('id', params.id).update(data)

        return response.json({ cat });
    }

    /**
     * deleteCat
     */
    public async deleteCat({ response, params }: HttpContextContract) {
        // const cat = await Cat.findByOrFail('id', params.id);
        // cat.delete();

        const cat = await Database.from('cats').where('id', params.id).delete();

        return response.json({ cat });
    }
}