/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', 'CatsController.index')
// get cats
Route.get('/cats', 'CatsController.getAllCats')
// create cat
Route.post('/cats/create', 'CatsController.createCat')
// get cat by its id
Route.get('/cats/:id', 'CatsController.getCatById')
// get cat for edit 
Route.get('/cats/:id/edit', 'CatsController.getCatForEdit')
// update cat
Route.put('/cats/:id', 'CatsController.updateCat')
// delete cat
Route.delete('/cats/:id', 'CatsController.deleteCat')

